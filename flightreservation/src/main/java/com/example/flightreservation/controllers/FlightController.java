package com.example.flightreservation.controllers;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.flightreservation.entities.Flight;
import com.example.flightreservation.service.FlightService;

@Controller
public class FlightController {
	
	@Autowired
	private FlightService flightService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FlightController.class);
	
	@GetMapping("/findFlights")
	public String findFlights() {
		LOGGER.info("inside findFlights()-get");
		return "findFlights";
	}
	
	@PostMapping("/findFlights")
	public String findFlights(@RequestParam("from")String from, 
			@RequestParam("to")String to, 
			@DateTimeFormat(pattern = "MM-dd-yyyy") Date departureDate,
			ModelMap modelMap) {
		LOGGER.info("inside findFlights()-post from " + from + " to: " + to + " departure Date " + departureDate.toString());
		List<Flight> findFlights = flightService.findFlights(from, to, departureDate);
		modelMap.addAttribute("flights", findFlights);
		LOGGER.info("Flight found here " + findFlights());
		return "displayFlights";
	}
	
	@RequestMapping("/admin/showAddFlights")
	public String showAddFlights() {
		return "addFlight";
	}
	
}
