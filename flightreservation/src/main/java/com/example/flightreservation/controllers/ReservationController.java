package com.example.flightreservation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.flightreservation.dto.ReservationRequest;
import com.example.flightreservation.entities.Flight;
import com.example.flightreservation.entities.Reservation;
import com.example.flightreservation.service.FlightService;
import com.example.flightreservation.service.ReservationService;

@Controller
public class ReservationController {
	
	@Autowired
	private ReservationService reservationService;
	
	@Autowired
	private FlightService flightService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReservationController.class);
	
	@RequestMapping("/showCompleteReservation")
	public String showCompleteReservation(@RequestParam("flightId") Long flightId , ModelMap modelMap) {
		LOGGER.info("showCompleteReservation() " +  flightId);
		Flight flight = flightService.findFlightById(flightId);
		modelMap.addAttribute("flight", flight);
		LOGGER.info("Flight is:  " +  flight);
		return "completeReservation";
	}
	
	@PostMapping("/completeReservation")
	public String completeReservation(@ModelAttribute ReservationRequest request, ModelMap modelMap) {
		
		Reservation reservation	 = reservationService.bookFlight(request);
		
		modelMap.addAttribute("msg", "Reservation created successfully and the id is : " + reservation.getId());
		
		return "reservationConfirmation";
	}
	
}
