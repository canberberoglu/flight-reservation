package com.example.flightreservation.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.flightreservation.entities.Flight;
import com.example.flightreservation.repos.FlightRepository;

@Service
@Transactional
public class FlightServiceImpl implements FlightService {

	private FlightRepository flightRepository;
	
	@Autowired
	public void setFlightRepository(FlightRepository flightRepository) {
		this.flightRepository = flightRepository;
	}

	@Override
	public Flight findFlightById(Long id) {
		Optional<Flight> optional = flightRepository.findById(id);
		if(optional.isPresent()) {
			return optional.get();
		}
		return null;	}

	@Override
	public List<Flight> findAllFlights() {
		return flightRepository.findAll();
	}

	@Override
	public Flight saveFlight(Flight flight) {
		return flightRepository.save(flight);
	}

	@Override
	public Flight updateFlight(Flight flight) {
		return flightRepository.save(flight);
	}

	@Override
	public void deleteFlight(Flight flight) {
		flightRepository.delete(flight);
	}

	@Override
	public List<Flight> findFlights(String from, String to, Date departureDate) {
		return flightRepository.findFlights(from, to, departureDate);
	}

}
