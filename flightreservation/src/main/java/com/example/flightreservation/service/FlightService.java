package com.example.flightreservation.service;

import java.util.Date;
import java.util.List;

import com.example.flightreservation.entities.Flight;

public interface FlightService {
	
	Flight findFlightById(Long id);
	
	List<Flight> findAllFlights();
	
	Flight saveFlight(Flight flight);
	
	Flight updateFlight(Flight flight);
	
	void deleteFlight(Flight flight);

	List<Flight> findFlights(String from, String to, Date departureDate);
	
}
