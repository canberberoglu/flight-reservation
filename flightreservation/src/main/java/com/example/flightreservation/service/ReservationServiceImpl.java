package com.example.flightreservation.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.flightreservation.dto.ReservationRequest;
import com.example.flightreservation.entities.Flight;
import com.example.flightreservation.entities.Passenger;
import com.example.flightreservation.entities.Reservation;
import com.example.flightreservation.repos.FlightRepository;
import com.example.flightreservation.repos.PassengerRepository;
import com.example.flightreservation.repos.ReservationRepository;
import com.example.flightreservation.util.EmailUtil;
import com.example.flightreservation.util.PDFGenerator;

@Service
@Transactional
public class ReservationServiceImpl implements ReservationService {
	@Autowired
	private FlightRepository flightRepository;

	@Autowired
	private PassengerRepository passengerRepository;

	@Autowired
	private ReservationRepository reservationRepository;

	@Autowired
	private PDFGenerator pdfGenerator;

	@Autowired
	private EmailUtil emailUtil;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReservationServiceImpl.class);

	@Override
	public Reservation bookFlight(ReservationRequest request) {

		// Make payment

		Optional<Flight> flight = flightRepository.findById(request.getFlightId());
		
		LOGGER.info("Fetching flight for flight id : " + request.getFlightId());

		Passenger passenger = new Passenger();
		passenger.setFirstName(request.getPassengerFirstName());
		passenger.setLastName(request.getPassengerLastName());
		passenger.setPhone(request.getPassengerPhone());
		passenger.setEmail(request.getPassengerEmail());
		LOGGER.info("Saved the passenger " + passenger);
		Passenger savedPassenger = passengerRepository.save(passenger);

		Reservation reservation = new Reservation();
		reservation.setPassenger(savedPassenger);
		reservation.setFlight(flight.get());
		reservation.setCheckedIn(false);
		
		LOGGER.info("Saving the reservation " + reservation);
		Reservation savedReservation = reservationRepository.save(reservation);
		
		String filePath = "C:\\Users\\can\\Desktop\\reservation" + savedReservation.getId() + ".pdf";
		
		LOGGER.info("Generating the itinerary pdf");
		pdfGenerator.generateItinerary(savedReservation,
				filePath);
		LOGGER.info("Emailing the itinerary");
		emailUtil.sendItinerary(savedPassenger.getEmail(), filePath);

		return savedReservation;
	}

}
