package com.example.flightreservation.service;

import java.util.List;

import com.example.flightreservation.entities.User;

public interface UserService {
	
	User findUserById(Long id);
	
	List<User> findAllUser();
	
	User saveUser(User user);
	
	User updateUser(User user);
	
	void deleteUser(User user);
 	
}
