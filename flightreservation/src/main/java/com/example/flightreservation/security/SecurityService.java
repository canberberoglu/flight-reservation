package com.example.flightreservation.security;

public interface SecurityService {
	
	boolean login(String username, String password);
	
}
